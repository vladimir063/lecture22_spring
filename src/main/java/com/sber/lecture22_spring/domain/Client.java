package com.sber.lecture22_spring.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Client {

    private String accountNumber;

    private double balance;

    private String pinCode;
}
