package com.sber.lecture22_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lecture22SpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lecture22SpringApplication.class, args);
    }

}
