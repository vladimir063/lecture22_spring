package com.sber.lecture22_spring.services;

import com.sber.lecture22_spring.domain.TransferRequest;
import com.sber.lecture22_spring.domain.TransferResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

public interface TerminalService {

    void transferAmountValidation(double total);

    double checkBalance();

    boolean authentication(String pinCode);

    void withdrawMoney(TransferRequest transferRequest);

    void putMoney(TransferRequest transferRequest);
}
