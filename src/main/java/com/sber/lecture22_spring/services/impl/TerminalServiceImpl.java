package com.sber.lecture22_spring.services.impl;

import com.sber.lecture22_spring.domain.Client;
import com.sber.lecture22_spring.domain.TransferRequest;
import com.sber.lecture22_spring.domain.TransferResponse;
import com.sber.lecture22_spring.exeption.TransferAmountValidationException;
import com.sber.lecture22_spring.services.TerminalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TerminalServiceImpl implements TerminalService {

    Client client = new Client("42534521", 7123113, "0000");


    @Override
    public void transferAmountValidation(double total) {
        if (total % 100 != 0) {
            log.error("Ошибка при выполнении перевода. Сумма перевода быть кратна 100. Повторите перевод");
            throw new TransferAmountValidationException("Ошибка при выполнении перевода. Сумма перевода быть кратна 100. Повторите перевод");
        }
        log.info("transferAmountValidation проверка пройдена");
    }

    @Override
    public double checkBalance() {
        return client.getBalance();
    }

    @Override
    public boolean authentication(String pinCode) {
        return client.getPinCode().equalsIgnoreCase(pinCode);
    }

    @Override
    public void withdrawMoney(TransferRequest transferRequest) {
        transferAmountValidation(transferRequest.getAmount());
        client.setBalance(client.getBalance() - transferRequest.getAmount());
    }

    @Override
    public void putMoney(TransferRequest transferRequest) {
        transferAmountValidation(transferRequest.getAmount());
        client.setBalance(client.getBalance() + transferRequest.getAmount());
    }
}
