package com.sber.lecture22_spring.controller;

import com.sber.lecture22_spring.domain.AuthenticationRequest;
import com.sber.lecture22_spring.domain.BalanceResponse;
import com.sber.lecture22_spring.domain.TransferRequest;
import com.sber.lecture22_spring.domain.TransferResponse;
import com.sber.lecture22_spring.exeption.AccountIsLockedException;
import com.sber.lecture22_spring.exeption.AuthenticationException;
import com.sber.lecture22_spring.services.TerminalService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequiredArgsConstructor
@Slf4j
public class TerminalController {

    private final TerminalService terminalService;

    private static final String SUCCESS_STATUS = "success";
    private static final String ERROR_STATUS = "error";
    private static final int CODE_SUCCESS = 100;
    private static final int AUTH_FAILURE = 102;
    private volatile boolean isAuthenticated = false;

    private volatile LocalTime timeLock = LocalTime.now();
    private AtomicInteger countErrorPin = new AtomicInteger(0);


    {
        new Thread(() -> {
            while (true) {
                if (countErrorPin.get() == 3) {
                    timeLock = LocalTime.now().plusSeconds(5);
                    countErrorPin.set(0);
                }
            }
        }).start();
    }

    @GetMapping("/checkBalance")
    public BalanceResponse checkBalance() {
        if (!isAuthenticated) {
            throw new AuthenticationException("Вы не авторизованы, доступ запрещен. Введите пин код");
        }
        double balance = terminalService.checkBalance();
        log.info("баланс клиента равен " + balance);
        return new BalanceResponse(balance);
    }

    @PostMapping("/authentication")
    public TransferResponse authentication(@RequestBody AuthenticationRequest authenticationRequest) {
        if (LocalTime.now().compareTo(timeLock) < 0) {
            throw new AccountIsLockedException("Вы ввели 3 раза неправильный пин код. Аккаунт заблакирован до " + timeLock.toString());
        }
        if (terminalService.authentication(authenticationRequest.getPinCode())) {
            isAuthenticated = true;
            log.info("пин код принят");
            return new TransferResponse(SUCCESS_STATUS, CODE_SUCCESS);
        } else {
            countErrorPin.incrementAndGet();
            log.info("пин код неверный");
            return new TransferResponse(ERROR_STATUS, AUTH_FAILURE);
        }
    }

    @PostMapping("/withdraw_money")
    public TransferResponse withdrawMoney(@RequestBody TransferRequest transferRequest) {
        if (!isAuthenticated) {
            throw new AuthenticationException("Вы не авторизованы, доступ запрещен. Введите пин код");
        }
        terminalService.withdrawMoney(transferRequest);
        return new TransferResponse(SUCCESS_STATUS, CODE_SUCCESS);
    }

    @PostMapping("/put_money")
    public TransferResponse putMoney(@RequestBody TransferRequest transferRequest) {
        if (!isAuthenticated) {
            throw new AuthenticationException("Вы не авторизованы, доступ запрещеню. Введите пин код");
        }
        terminalService.putMoney(transferRequest);
        return new TransferResponse(SUCCESS_STATUS, CODE_SUCCESS);
    }
}
