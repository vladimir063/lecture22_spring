package com.sber.lecture22_spring.exeption;

public class AccountIsLockedException extends RuntimeException {

    public AccountIsLockedException(String message) {
        super(message);
    }
}
