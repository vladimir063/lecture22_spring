package com.sber.lecture22_spring.exeption;

public class TransferAmountValidationException extends RuntimeException {

    public TransferAmountValidationException(String message) {
        super(message);
    }
}
